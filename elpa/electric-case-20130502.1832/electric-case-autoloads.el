;;; electric-case-autoloads.el --- automatically extracted autoloads
;;
;;; Code:


;;;### (autoloads (electric-case-mode) "electric-case" "electric-case.el"
;;;;;;  (20869 61172 0 0))
;;; Generated autoloads from electric-case.el

(autoload 'electric-case-mode "electric-case" "\
Toggle electric-case-mode

\(fn &optional ARG)" t nil)

;;;***

;;;### (autoloads nil nil ("electric-case-pkg.el") (20869 61172 408885
;;;;;;  0))

;;;***

(provide 'electric-case-autoloads)
;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; coding: utf-8
;; End:
;;; electric-case-autoloads.el ends here
